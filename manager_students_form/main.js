var students = [
    {
        id: 1,
        name: 'Quang Huy',
        address: 'Ha Noi',
        age: 18,
        phone: '0979.690.890'
    },
    {
        id: 2,
        name: 'Quang Hùng',
        address: 'Ha Noi',
        age: 19,
        phone: '0979.690.891'
    },
    {
        id: 3,
        name: 'Quang Hưng',
        address: 'Ha Noi',
        age: 20,
        phone: '0979.690.892'
    }
]
var editMode = false;
var tmpIndex;

function enableEditMode() {
    editMode = true;
}

function disableEditMode() {
    editMode = false;
}

window.addEventListener('DOMContentLoaded', (event) => {
    renderStudents();
});

function renderStudents() {
    // var listStudent = document.getElementById('list-student');
    var html = '';
    for (var i = 0; i < students.length; i++) {
        var student = students[i];
        html += `
        <div class="col-md-4 mt-4">
            <div class="card" style="min-height: 270px">
                <div class="card-body">
                    <p class="card-text">Name: ${student.name}</p>
                    <p class="card-text">Age: ${student.age}</p>
                    <p class="card-text">Address: ${student.address}</p>
                    <p class="card-text">Phone: ${student.phone}</p>
                    <button type="button" class="btn btn-primary btn-sm" onclick="editStudent(${i})">Edit</button>
                    <button type="button" class="btn btn-danger btn-sm" onclick="deleteStudent(${i})">Delete</button>
                </div>
            </div>
        </div>`
    }
    // listStudent.innerHTML = html;
    setHTML('list-student', html)
}

function setHTML(element, value) {
    var element = document.getElementById(element);
    element.innerHTML = value;
}

function getInputElement(element) {
    var element = document.getElementById(element);
    return element;
}

function setInputValue(element, value) {
    var element = document.getElementById(element);
    element.value = value;
}

var createStudent = function () {
    if (editMode) {
        handleEditStudent();
    } else {
        var name = getInputElement("name").value;
        var age = getInputElement("age").value;
        var address = getInputElement("address").value;
        var phone = getInputElement("phone").value;

        var newStudent = {
            name,
            age,
            address,
            phone,
        }
        students.push(newStudent);
        renderStudents();
        resetForm();
    }
}

function deleteStudent(index) {
    students.splice(index, 1);
    renderStudents();
}

function editStudent(index) {
    var student = students[index];

    setInputValue("name", student.name);
    setInputValue("age", student.age);
    setInputValue("address", student.address);
    setInputValue("phone", student.phone);

    enableEditMode();
    setHTML("create_btn", "Save");
    tmpIndex = index;
}

function handleEditStudent() {
    var name = getInputElement("name").value;
    var age = getInputElement("age").value;
    var address = getInputElement("address").value;
    var phone = getInputElement("phone").value;

    var newEditStudent = {
        name,
        age,
        address,
        phone
    }
    editInputStudent(newEditStudent, tmpIndex);

    renderStudents();
    disableEditMode();
    setHTML("create_btn", "Create");
    resetForm();
}

function editInputStudent(student, index) {
    students[index] = student;
}

function resetForm() {
    setInputValue("name", "");
    setInputValue("age", "");
    setInputValue("address", "");
    setInputValue("phone", "");
}

