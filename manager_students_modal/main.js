var students = [
    {
        id: 1,
        name: 'Quang Huy',
        address: 'Ha Noi',
        age: 18,
        phone: '0979.690.890'
    },
    {
        id: 2,
        name: 'Quang Hùng',
        address: 'Ha Noi',
        age: 19,
        phone: '0979.690.891'
    },
    {
        id: 3,
        name: 'Quang Hưng',
        address: 'Ha Noi',
        age: 20,
        phone: '0979.690.892'
    }
]

var editMode = false;
var tmpIndex;

function enableEditMode() {
    editMode = true;
}

function disableEditMode() {
    editMode = false;
}

//
window.addEventListener('DOMContentLoaded', (event) => {
    renderStudents();
});

function renderStudents() {
    var html = '';
    for (var i = 0; i < students.length; i++) {
        var student = students[i];
        html +=
            `
        <tr>
            <td>${i + 1}</td>
            <td>${student.name}</td>
            <td>${student.age}</td>
            <td>${student.phone}</td>
            <td>${student.address}</td>
            <td>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal" onclick="editStudent(${i})">
            Edit
            </button>
            <button type="button" class="btn btn-danger btn-sm" onclick="deleteStudent(${i})">Delete</button>
            </td>
        </tr>
        `
    }
    setHTML('list-student', html)
}

function setHTML(element, value) {
    var element = document.getElementById(element);
    element.innerHTML = value;
}

function getInputElement(element) {
    var element = document.getElementById(element);
    return element;
}

function setInputValue(element, value) {
    var element = document.getElementById(element);
    element.value = value;
}

//
var createStudent = function () {
    if (editMode) {
        handleEditStudent();

    } else {
        var name = getInputElement("name").value;
        var age = getInputElement("age").value;
        var address = getInputElement("address").value;
        var phone = getInputElement("phone").value;

        var newStudent = {
            name,
            age,
            address,
            phone,
        }
        students.push(newStudent);
        renderStudents();
        resetForm();
    }

}

//
function deleteStudent(index) {
    var isConfirm = confirm('Are you delete?');

    if (isConfirm) {
        students.splice(index, 1);
        renderStudents();
    }
}

//
function editStudent(index) {
    var student = students[index];

    setInputValue("name", student.name);
    setInputValue("age", student.age);
    setInputValue("address", student.address);
    setInputValue("phone", student.phone);

    enableEditMode(); create_btn
    setHTML("create_btn", "Save");
    tmpIndex = index;
}

//
function handleEditStudent() {
    var name = getInputElement("name").value;
    var address = getInputElement("address").value;
    var age = getInputElement("age").value;
    var phone = getInputElement("phone").value;

    var newEditStuent = {
        name,
        address,
        age,
        phone
    }

    editInputStudent(newEditStuent, tmpIndex);

    renderStudents();
    disableEditMode();
    setHTML("create_btn", "Create");

    resetForm();
}

function editInputStudent(student, index) {
    students[index] = student;
}

//
function resetForm() {
    setInputValue("name", "");
    setInputValue("age", "");
    setInputValue("address", "");
    setInputValue("phone", "");
}

